<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Я - Энтузиаст</title>
    <meta property="og:title" content="Я - Энтузиаст" />
    <meta property="og:description" content="Сможешь лучше?" />
    <meta property="og:image" content="src/images/soc/enthusiast.png" />
</head>
<body>

<script>
    location.href = '/'
</script>
</body>
</html>