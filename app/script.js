import Bundler from './modules/bundler/index'
import Listener from './modules/listener/index'
import Store from './modules/storage/index'
import Social from './modules/storage/social'
import 'babel-polyfill'
import 'isomorphic-fetch'
import 'custom-event-polyfill'

Store.state.events.forEach(event => {
    Listener.initEvent(event)
})
let scoreGame = 0
let scorePrecent = 0
let type = 0
let href = 0

function getTitul(score, gameScore) {
    let num = textSwitch(score)
    let socials = document.querySelectorAll('.soc-share')
    Array.prototype.forEach.call(socials, soc => {
        type = 'social--vk'
        href = location.href + `share${num}.php`
        if (soc.classList.contains('vk')) {
            type = 'social--vk'
        }
        if (soc.classList.contains('f')) {
            type = 'social--facebook'
        }
        if (soc.classList.contains('ok')) {
            type = 'social--ok'
        }
        const socClick = e => {
            if (e.target.classList.contains('vk')) {
                type = 'social--vk'
            }
            if (e.target.classList.contains('f')) {
                type = 'social--facebook'
            }
            if (e.target.classList.contains('ok')) {
                type = 'social--ok'
            }
            bundler.showEndGameScreen(gameScore, score)
        }
        soc.removeEventListener('click', socClick)
        soc.addEventListener('click', socClick)
    })
}

function textSwitch(score) {
    if (score <= 30) {
        return 3
    }
    if (score > 30 && score < 50) {
        return 1
    }
    if (score >= 50 && score < 70) {
        return 5
    }
    if (score >= 70 && score < 90) {
        return 2
    }
    if (score >= 90) {
        return 4
    }
}

const bundler = new Bundler()
bundler.firstLoad()
    .then(() => {
        bundler.drawMainScreen()
    })
    .catch(err => console.error(err))
// Пользователь выбрал игру
document.addEventListener(Store.state.events[0].name, (e) => {
    bundler.startGame(e.detail.nImage.props.id)
})
// Пользователь нажал паузу
document.addEventListener(Store.state.events[1].name, () => {

})

// Пользователь закончил игру
document.addEventListener('gameEnd', (e) => {
    scoreGame = e.detail.score
    const maxScore = e.detail.maxScore
    scorePrecent = (scoreGame * 100) / maxScore
    getTitul(scorePrecent, scoreGame)
    bundler.gameClose()
        .then(() => {
            bundler.drawLastScreen(scoreGame, scorePrecent)
        })
})

document.addEventListener('leaveGame', (e) => {
    bundler.gameClose()
        .then(() => {
            bundler.drawMainScreen()
        })
})

// Играть снова
document.querySelector('#symphony-app .symphony-result .symphony-close').addEventListener('click', () => {
    document.querySelector('#symphony-app .symphony-result').classList.remove('active')
    let layer = document.querySelector('#symphony-app .symphony-layer')
    layer.classList.remove('active')
})

// Показать последний экран
document.querySelector('#symphony-app .symphony-result .symphony-button').addEventListener('click', () => {
    Social.shareSocial(type, 0, href)
})


// TODO: paste signature
