import Anim from '../canvas/anim'
export default class Listener {
    constructor(object) {
        this.object = object
        this.collector = []
    }

    event(nameEvent) {
        const events = {
            click: {
                name: 'click',
                mobileName: 'tap'
            },
            mouseover: {
                name: 'mouseover',
                mobileName: 'touchstart'
            },
            mouseout: {
                name: 'mouseout',
                mobileName: 'touchend'
            }
        }
        return `${events[nameEvent].name} ${events[nameEvent].mobileName}`
    }

    hoverCursor() {
        this.addListener(this.event('mouseover'), () => {
            Anim.setCursor('pointer')
        })
        this.addListener(this.event('mouseout'), () => {
            Anim.setCursor('default')
        })
    }

    onMouseMove(props, callback) {
        this.collector.push({
            obj: {
                x: props.x,
                y: props.y,
                w: props.w,
                h: props.h
            },
            callback
        })
        this.addListener('mousemove')
    }

    addListener(type, func) {
        const list = this.object.on(type, evt => {
            func(evt)
        })
        this.collector.push(list)
    }

    static initEvent(ev) {
        let event = document.createEvent('Event')
        event.initEvent(ev.name, true, true)
        ev.event = event
    }

    /**
     * Регистрируем нажатие клавиш
     * @param params {Array}
     * @param params.object.key - код клавиши
     * @param params.object.event - callback
     */
    static registerKeyboardEvents(params) {
        document.addEventListener('keydown', function (e) {
            let keyCode = e.keyCode
            let findedObject = params.find(object => object.key === keyCode)
            if (findedObject) {
                findedObject.event()
            }
        })
    }
}
