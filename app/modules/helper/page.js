export default class Page {
    static isMobile() {
        return screen.width < 800
    }

    static isVerticalOrientation() {
        return screen.width < 450
    }

    static bindCustomScroll(params) {
        const container = document.getElementById(params.container)
        const content = document.getElementById(params.content)
        const scroll = document.getElementById(params.scroll)
        const isFirefos = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1)

        content.addEventListener('scroll', (e) => {
            // scroll.style.height = container.clientHeight * content.clientHeight / content.scrollHeight + 'px'
            scroll.style.top = container.clientHeight * content.scrollTop / content.scrollHeight + 'px'
        })
        if (isFirefos) {
            const settimeOut = timeOut => {
                if (timeOut) {
                    return false
                }
                setTimeout(() => {
                    document.body.style.overflow = 'auto'
                    clearTimeout(timeOut)
                }, 1000)
            }
            let timer = 0
            content.onwheel = e => {
                document.body.style.overflow = 'hidden'
                content.scrollTop += e.deltaY * 4
                settimeOut(timer)
            }
        }
        let event = new CustomEvent('scroll')

        window.addEventListener('resize', content.dispatchEvent.bind(content, event))
        content.dispatchEvent(event)

        scroll.addEventListener('mousedown', function (start) {
            start.preventDefault()
            let y = scroll.offsetTop
            let onMove = (end) => {
                let delta = end.pageY - start.pageY
                scroll.style.top = Math.min(container.clientHeight - scroll.clientHeight, Math.max(0, y + delta)) + 'px'
                content.scrollTop = (content.scrollHeight * scroll.offsetTop / container.clientHeight)
            }
            document.addEventListener('mousemove', onMove)
            document.addEventListener('mouseup', function () {
                document.removeEventListener('mousemove', onMove)
            })
        })
    }
}
