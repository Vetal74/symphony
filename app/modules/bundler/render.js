import Canvas from './../canvas/index'
import Anim from './../canvas/anim'
import Storage from '../storage'
export default class Render {
    /**
     * Рендер всячины на моник
     * @param canvasSize
     */
    constructor(canvasSize) {
        this.canvas = new Canvas(canvasSize.width, canvasSize.height)
    }

    /**
     * Плавно удаляем элементы со слоя
     * @param zIndex
     * @param duration
     * @returns {Promise}
     */
    removeLayerItems(zIndex, duration = 0.2) {
        const layer = this.canvas.getLayer(zIndex)
        let layerElems = layer.getChildren()
        layerElems.forEach(elm => {
            elm.tweenFade = new Anim(elm, layer, {
                opacity: 0,
                y: elm.getY() + 50,
                duration
            }).tween()
            if (elm.tween) {
                elm.tween.pause()
            }
            elm.tweenFade.play()
        })
        return new Promise(resolve => {
            setTimeout(() => {
                layer.removeChildren()
                resolve()
            }, duration * 1500)
        })
    }

    /**
     * Отрисовка изображения на канвас
     * @param image - изображение с объектом типа Image
     * @param x
     * @param y
     * @param sizes
     * @param zIndex
     * @returns {*} - KonvaImage
     */
    renderImage(image, x = image.position.x, y = image.position.y, sizes = {
        width: image.size.width,
        height: image.size.height
    }, zIndex = image.zIndex, rotate = 0) {
        let konvaImage = this.canvas.drawImageToCanvas(image.imageObj, {x, y}, sizes, zIndex, rotate)
        return Object.assign(konvaImage, {props: image})
    }

    /**
     * Подготовка и вставка изображений в canvas
     * @param {Array} images изображения из базы
     * @param {string} type тип изображения
     */
    renderImages(images = []) {
        try {
            if (!images.length) {
                throw new Error('Не переданы изображения!')
            }
            let konvaImages = []
            images.forEach(image => {
                konvaImages.push(this.renderImage(image))
            })
            return konvaImages
        } catch (err) {
            console.error(err)
        }
    }

    /**
     * Выводим на экран кружки
     * @param shape - параметры кружка
     * @returns {Array} - массив с кружками на холсте
     */
    renderShapes(shape) {
        if (!shape) {
            throw new Error('Не передан объект Shape в render')
        }
        let shapes = []
        let textShape = ['A', 'S', 'D', 'F', 'G']
        for (let i = 0; i < shape.count; i++) {
            let left = shape.styles.leftMargin + shape.styles.margin * i
            let circle = this.canvas.drawCircle({
                x: left,
                y: shape.position.y,
                radius: shape.styles.radius,
                fill: shape.styles.fill,
                stroke: shape.styles.stroke,
                strokeWidth: shape.styles.strokeWidth,
            }, shape.zIndex)
            this.renderText({
                text: textShape[i],
                fill: '#536176',
                fontSize: 20,
                y: shape.position.y - 9,
                x: left - 7
            }, shape.zIndex)
            let layer = this.canvas.getLayer(shape.zIndex)
            let animationFail = shape.animations.find(animation => animation.name === 'fail')
            circle.tweenFail = new Anim(circle, layer, Object.assign(animationFail.styles, {
                duration: animationFail.duration,
                onFinish() {
                    circle.tweenFail.reverse()
                }
            })).tween()
            shapes.push(circle)
        }
        return shapes
    }

    /**
     * Рисуем линии на холсте
     * @param line - объект линии с параметрами
     * @void
     */
    renderLines(line) {
        if (!line) {
            throw new Error('Не передан объект линии в рендер')
        }
        for (let i = 0; i < line.count; i++) {
            let left = line.styles.leftMargin + line.styles.margin * i
            this.canvas.drawLine({
                points: [left, line.position.startLine, left, line.position.endLine],
                stroke: line.styles.stroke,
                strokeWidth: line.styles.strokeWidth
            }, line.zIndex)
        }
    }

    /**
     * Рисуем балончики
     * @param balon
     * @returns {Array} - konva
     */
    renderBalons(balon) {
        let balons = []
        for (let i = 0; i < balon.length; i++) {
            let left = balon[i].styles.leftMargin + balon[i].styles.margin * i
            let konvaImage = this.renderImage(balon[i], left, undefined, undefined, undefined, (-5 * Math.PI))
            this.canvas.setCenter(konvaImage, {x: 'center', y: 'center'})

            konvaImage.hitFunc(function (context) {
                context.beginPath()
                context.rect(0, (-1) * (this.height()), this.width() * 1.2, this.height() * 2)
                context.closePath()
                context.fillStrokeShape(this)
            })
            const layer = this.canvas.getLayer(balon[i].zIndex)
            konvaImage.tweenRotate = new Anim(konvaImage, layer, {
                rotation: -2 * Math.PI,
                duration: 0.1
            }).tween()
            balons.push(konvaImage)
        }
        return balons
    }

    renderSpray(spray) {
        let sprays = []
        for (let i = 0; i < spray.count; i++) {
            let left = spray.styles.leftMargin + spray.styles.margin * i
            let konvaImage = this.renderImage(spray, left)
            let popAnim = spray.animations.find(anim => anim.name === 'pop')
            const layer = this.canvas.getLayer(spray.zIndex)

            this.canvas.setCenter(konvaImage, {x: Number(spray.size.width) - 50, y: Number(spray.size.height)})
            konvaImage.scale({x: 0, y: 0})
            konvaImage.tweenPop = new Anim(konvaImage, layer,
                Object.assign(popAnim.styles, {
                    onFinish: () => {
                        konvaImage.tweenPop.reverse()
                    }
                })
            ).tween()
            sprays.push(konvaImage)
        }
        return sprays
    }

    renderFruitSpray(fruitSpray) {
        let fruitSprays = []
        for (let i = 0; i < fruitSpray.count; i++) {
            let left = fruitSpray.styles.leftMargin + fruitSpray.styles.margin * i
            let konvaImage = this.renderImage(fruitSpray, left)
            let popAnim = fruitSpray.animations.find(anim => anim.name === 'pop')
            let goToProgress = fruitSpray.animations.find(anim => anim.name === 'goToProgress')
            const layer = this.canvas.getLayer(fruitSpray.zIndex)

            konvaImage.scale({x: 0, y: 0})
            this.canvas.setCenter(konvaImage, {
                x: Number(fruitSpray.size.width) - 50,
                y: Number(fruitSpray.size.height)
            })
            konvaImage.tweenGoToProgress = new Anim(konvaImage, layer,
                Object.assign(goToProgress.styles, {
                    onFinish: () => {
                        konvaImage.tweenGoToProgress.reset()
                        konvaImage.scale({x: 0, y: 0})
                    }
                })
            ).tween()
            konvaImage.tweenPop = new Anim(konvaImage, layer,
                Object.assign(popAnim.styles, {
                    onFinish: () => {
                        setTimeout(() => {
                            konvaImage.tweenGoToProgress.play()
                        }, 100)
                    }
                })
            ).tween()
            fruitSprays.push(konvaImage)
        }
        return fruitSprays
    }

    drawProgressLine(rect) {
        let konva = new Konva.Rect({
            x: rect.styles.x,
            y: rect.styles.y,
            height: rect.size.height,
            width: rect.size.width,
            offsetY: rect.size.height,
            fillLinearGradientColorStops: rect.styles.fillLinearGradientColorStops,
            fillLinearGradientStartPointY: rect.styles.fillLinearGradientStartPointY,
            fillLinearGradientEndPointY: rect.styles.fillLinearGradientEndPointY
        })
        konva.position({
            x: rect.styles.x,
            y: rect.styles.y
        })
        let layer = this.canvas.getLayer(rect.zIndex)
        layer.add(konva)
        konva.props = rect
        return konva
    }

    /**
     * Отрисовка фруктов и назначение анимаций
     * @param gameObjects
     * @param gameSettings
     * @returns {Array}
     */
    renderGameItems(gameObjects, gameSettings) {
        let fruits = []
        gameObjects.forEach((fruit, key) => {
            let left = fruit.styles.leftMargin + fruit.styles.margin * key
            let konvaImage = this.renderImage(fruit, left)
            konvaImage.cache()
            this.canvas.setCenter(konvaImage, {x: 'center', y: 'center'})

            const layer = this.canvas.getLayer(fruit.zIndex)

            // konvaImage.on('click tap', evt => {
            //     let eventClone = new CustomEvent('fruitClick', {
            //         detail: {key}
            //     })
            //     document.dispatchEvent(eventClone)
            //     return false
            // })

            let clones = []
            for (let j = 0; j < 10; j++) {
                let clone = konvaImage.clone()
                layer.add(clone)
                let animationTranslate = fruit.animations.find(animation => animation.name === 'translate')
                clone.on('click tap', evt => {
                    let eventClone = new CustomEvent('fruitClick', {
                        detail: {key}
                    })
                    document.dispatchEvent(eventClone)
                })
                clone.tweenTranslate = new Anim(clone, layer,
                    Object.assign(animationTranslate.styles, {
                        duration: gameSettings.speed,
                        onFinish: () => {
                            let eventClone = new CustomEvent('cloneEnd', {
                                detail: {clone}
                            })
                            document.dispatchEvent(eventClone)
                        }
                    })).tween()
                clone.cache()
                clones.push(clone)
            }

            fruits.push(clones)
        })
        return fruits
    }


    renderText(params, zIndex = 2) {
        let konvaText = this.canvas.drawText(params, zIndex)
        return konvaText
    }

    renderMotivationText(texts) {
        let konvaTexts = []
        texts.texts.forEach(text => {
            let konvaText = this.renderText(Object.assign({text: text.text}, text.styles, text.position), text.zIndex)
            const layer = this.canvas.getLayer(text.zIndex)
            konvaText.tweenVisible = new Anim(konvaText, layer, {
                y: text.position.y + 20,
                opacity: 1,
                duration: 0.1,
                onFinish: () => {
                    setTimeout(() => {
                        konvaText.tweenVisible.reverse()
                    }, 150)
                }
            }).tween()
            konvaTexts.push(konvaText)
        })
        return konvaTexts
    }

    renderRectangle(rect) {
        let konvaRect = this.canvas.drawRectangle(rect)
        konvaRect.props = rect
        return konvaRect
    }

    getCanvas() {
        return this.canvas
    }
}
