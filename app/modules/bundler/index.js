import Page from '../helper/page'
import Listener from './../listener/index'
import Anim from './../canvas/anim'
import Animations from './../canvas/animations'
import Render from './render'
import Loader from './../api/loader'
import AudioShaker from './../audio/index'
import Mehanic from './mehanic'
import API from '../api/index'

export default class Bundler {
    constructor() {
        this.render = 0
        this.animations = new Animations()
        this.mehanic = 0
        this.images = []
    }

    firstLoad() {
        let settings = new Loader('settings')
        settings.getAttrByName({id: 0, name: 'size'})
            .then(size => this.render = new Render(size))
        let images = new Loader('images')
        this.texts = new Loader('texts')
        return images.getObjectsByCategory('mainScreen')
            .then(needImages => images.loadImages(needImages))
            .then(loadedImages => this.images.push(loadedImages))
    }

    /**
     * Отрисовка главного меню
     */
    drawMainScreen() {
        try {
            let mainScreenImages = this.images.find(images => {
                return Boolean(images.filter(image => image.category === 'mainScreen'))
            })
            mainScreenImages = this.render.renderImages(mainScreenImages)
            let needTexts = {
                head: this.texts.getAttrById(0),
                choseGame: this.texts.getAttrById(1),
                scoreTable: this.texts.getAttrById(2)
            }
            needTexts.head.then(headText => {
                this.render.renderText(Object.assign(headText.texts[0].position,
                    headText.texts[0].styles, {text: headText.texts[0].text}), headText.texts[0].zIndex)
            })
            needTexts.choseGame.then(choseText => {
                choseText.texts.forEach(text => {
                    this.render.renderText(Object.assign(text.position, text.styles, {text: text.text}), text.zIndex)
                })
            })
            // needTexts.scoreTable.then(scoreText => {
            //     this.render.renderText(Object.assign(scoreText.texts[0].position,
            //         scoreText.texts[0].styles, {text: scoreText.texts[0].text}), scoreText.texts[0].zIndex)
            // })
            this.scoreTable = API.requestFromApi('GET', 'http://start-go.pro/symphony/base.php')
            this.scoreTable.then(response => {
                let tableHtml = ''
                setTimeout(() => {
                    this.table = document.querySelector('.rating-table-main #rate-content')
                    this.tableH = document.querySelector('.symphony-rating-h2')
                    if (document.querySelector('.rating-table-main')) {
                        document.querySelector('.rating-table-main').classList.remove('hide')
                    }
                    if (this.tableH) {
                        this.tableH.classList.remove('hide')
                    }
                    response.forEach((scoreItem, key) => {
                        scoreItem.key = key + 1
                        tableHtml += this.scoreTemplate(scoreItem)
                    })
                    this.table.innerHTML = tableHtml
                    Page.bindCustomScroll({
                        container: 'scroll-container',
                        content: 'rate-content',
                        scroll: 'scroll'
                    })
                    Page.bindCustomScroll({
                        container: 'scroll-container1',
                        content: 'rate-content1',
                        scroll: 'scroll1'
                    })
                }, 300)
            })
            this.drawNotes()
            this.animations.setAnimationMainScreen(mainScreenImages, this.render.getCanvas())
        } catch (err) {
            console.error(err)
        }
    }

    drawNotes() {
        let notes = new Loader('images')
        let tracks = new Loader('audio-tracks')
        let noteImages = {}
        let noteImageObjects = {}
        let tracksArray = []
        notes.getObjectsByCategory('preview')
            .then(result => {
                noteImages = result
                return notes.loadImages(noteImages)
            })
            .then(result => {
                noteImageObjects = result
                return tracks.getAttrs()
            })
            .then(result => {
                let tileImages = this.images[0].filter(image => image.podcat === 'game-preview')
                tracksArray = result
                const drawNote = (i, k, left, fill) => {
                    for (i; i < k; i++) {
                        this.render.renderImage(noteImageObjects[fill], left, 475, {
                            width: 13,
                            height: 17
                        })
                        left += 20
                    }
                    return left
                }
                tileImages.forEach((tileImage, key) => {
                    let left = tileImage.position.x
                    let hard = tracksArray[key].hard
                    left = drawNote(0, hard, left, 1)
                    drawNote(hard, 5, left, 0)
                })
            })
        // Отрисовка нот
    }

    startGame(selectedGame = 0) {
        this.selectedGameOld = selectedGame
        setTimeout(() => {
            if (document.querySelector('.rating-table-main')) {
                document.querySelector('.rating-table-main').classList.add('hide')
            }
            if (this.tableH) {
                this.tableH.classList.add('hide')
            }
        }, 300)
        let trackSettings = new Loader('audio-tracks')
        let promises = [] // Объединяем промисы
        trackSettings.getAttrById(selectedGame).then(trackSet => {
            let figures = new Loader('figure')
            let gameSettings = new Loader('game-settings')
            let trackMap = new Loader(trackSet.map)
            let gameImages = new Loader('images')
            let audio = new AudioShaker(trackSet.src, {
                timeStart: trackSet.timeStart
            })
            let audioPshik = new AudioShaker(trackSet.pshikSrc, {}, 0.2)
            promises[0] = this.render.removeLayerItems(1)
            promises[1] = figures.getAttrs()
            promises[2] = gameImages.getObjectsByCategory('game')
                .then(objects => gameImages.loadImages(objects))
            promises[3] = trackMap.getAttrs()
            promises[4] = gameSettings.getAttrs()
            promises[5] = audio
            promises[6] = audioPshik
            promises[7] = this.texts.getAttrById(3)
            this.preloaderActive()
            Promise.all(promises)
                .then(result => {
                    this.preloaderActive(false)
                    let shapes = this.drawGameFigures(result[1])
                    let balons = this.drawBalons(result[2], trackSet)
                    const gameItems = this.drawGameItems(result[2], result[4], trackSet)
                    const spray = this.drawSpray(result[2])
                    const fruitSpray = this.drawFruitSpray(result[2])
                    const progress = result[2].find(item => Number(item.id) === 8)
                    const textMotivation = this.render.renderMotivationText(result[7])
                    let progressLine = result[1].find(item => Number(item.id) === 2)
                    progressLine = this.render.drawProgressLine(progressLine)
                    const progressEffects = result[2].find(item => Number(item.id) === 12)
                    this.render.renderImage(progress)
                    setTimeout(() => {
                        this.render.renderImage(progressEffects)
                    }, 100)
                    this.mehanic = new Mehanic({
                        audio: result[5],
                        gameItems,
                        trackMap: result[3],
                        gameSettings: result[4],
                        textRender: this.render.renderText.bind(this.render),
                        audioPshik: result[6],
                        balons,
                        sprays: spray,
                        fruitSprays: fruitSpray,
                        progressLine,
                        shapes,
                        textMotivation
                    })
                    this.mehanic.start()
                    this.keyboardGame()
                })
                .catch(err => console.error(err))
        })
    }

    showEndGameScreen(score, scorePrecent) {
        let layer = document.querySelector('#symphony-app .symphony-layer')
        layer.classList.add('active')
        let results = document.querySelector('#symphony-app .symphony-result')
        results.classList.add('active')
        let titul = results.querySelector('h3')
        titul.innerHTML = this.textSwitch(scorePrecent, 'Я - ')
        let scoreContainer = results.querySelector('.score')
        scoreContainer.innerHTML = score
    }

    drawLastScreen(score, scorePrecent) {
        let share = document.querySelector('#symphony-app .symphony-share')
        share.classList.add('active')
        let titul = share.querySelector('.titul')
        titul.innerHTML = this.textSwitch(scorePrecent, 'Я - ')
        let scoreContainer = share.querySelector('.score-share')
        scoreContainer.innerHTML = score
        let gameRepeat = share.querySelector('.button-main-screen')
        let musicRepeat = share.querySelector('.button-repeat')

        const drawTable = name => {
            let table = share.querySelector('.rating-table #rate-content1')
            let tableHtml = ''
            this.scoreTable.then((scoreLine) => {
                scoreLine.forEach((line, key) => {
                    let active = ''
                    line.key = key + 1
                    if (name && name === line.name) {
                        active = 'active'
                    }
                    tableHtml += this.scoreTemplate(line, active)
                })
                table.innerHTML = tableHtml
                if (name) {
                    setTimeout(() => {
                        let activeLine = table.querySelector('.active')
                        table.scrollTop = activeLine.offsetTop - 50
                    }, 700)
                }
            })
        }
        drawTable()
        gameRepeat.onclick = () => {
            share.classList.remove('active')
            this.drawMainScreen()
        }
        musicRepeat.onclick = () => {
            share.classList.remove('active')
            this.startGame(this.selectedGameOld)
        }
        let save = share.querySelector('.symphony-right .symphony-button')
        let input = share.querySelector('.symphony-right input')
        save.classList.remove('hide')
        input.disabled = false
        let disInp = true
        save.onclick = () => {
            let name = input.value
            if (!input.value.length || !disInp) return false
            API.setFromApi('http://start-go.pro/symphony/base-post.php', {name, score})
                .then(result => {
                    input.value = ''
                    input.disabled = true
                    disInp = false
                    save.classList.add('hide')
                    this.scoreTable = API.requestFromApi('GET', 'http://start-go.pro/symphony/base.php')
                    drawTable(name)
                })
                .catch(err => {
                    input.value = ''
                    input.disabled = true
                    disInp = false
                    save.classList.add('hide')
                    this.scoreTable = API.requestFromApi('GET', 'http://start-go.pro/symphony/base.php')
                    drawTable(name)
                    // share.classList.remove('active')
                    // this.drawMainScreen()
                })
        }
    }

    gameClose() {
        if (!this.mehanic) {
            let layer = document.querySelector('#symphony-app .symphony-layer')
            layer.classList.remove('active')
            let results = document.querySelector('#symphony-app .symphony-result')
            results.classList.remove('active')
            return new Promise(resolve => resolve())
        }
        let pauseButton = document.querySelector('#symphony-app .pause-button')
        pauseButton.classList.remove('active')
        let promises = [
            this.render.removeLayerItems(1),
            this.render.removeLayerItems(2),
            this.render.removeLayerItems(3),
            this.render.removeLayerItems(4)
        ]
        return Promise.all(promises)
            .then(() => {
                this.mehanic.destroy()
                this.mehanic = 0
                let layer = document.querySelector('#symphony-app .symphony-layer')
                layer.classList.remove('active')
                let results = document.querySelector('#symphony-app .symphony-result')
                results.classList.remove('active')
                return true
            })
    }

    preloaderActive(show = true) {
        let layout = document.querySelector('#symphony-app .symphony-layer')
        let preloader = document.querySelector('#symphony-app .symphony-preloader')
        if (show) {
            layout.classList.add('active')
            preloader.classList.add('active')
        } else {
            layout.classList.remove('active')
            preloader.classList.remove('active')
        }
    }

    scoreTemplate({key, name, score}, active = '') {
        return `<div class="line ${active}">
                        <div class="name">${key}. ${name}</div>
                        <div class="score">${score} очков</div>
                    </div>`
    }

    textSwitch(score, dop = 'Вы - ') {
        if (score <= 30) {
            return dop + 'энтузиаст'
        }
        if (score > 30 && score < 50) {
            return dop + 'юное дарование'
        }
        if (score >= 50 && score < 70) {
            return dop + 'настоящий артист'
        }
        if (score >= 70 && score < 90) {
            return dop + 'виртуоз'
        }
        if (score >= 90) {
            return dop + 'музыкант от бога'
        }
        return dop + 'энтузиаст'
    }

    keyboardGame() {
        const mehanic = this.mehanic
        Listener.registerKeyboardEvents([
            {
                key: 65,
                event() {
                    mehanic.lineClick(0)
                }
            },
            {
                key: 83,
                event() {
                    mehanic.lineClick(1)
                }
            },
            {
                key: 68,
                event() {
                    mehanic.lineClick(2)
                }
            },
            {
                key: 70,
                event() {
                    mehanic.lineClick(3)
                }
            },
            {
                key: 71,
                event() {
                    mehanic.lineClick(4)
                }
            }
        ])
    }

    drawGameFigures(figures) {
        let shapes = figures.find(figure => figure.id === 0)
        let lines = figures.find(figure => figure.id === 1)
        this.render.renderLines(lines)
        shapes = this.render.renderShapes(shapes)
        shapes.forEach((shape, key) => {
            let listener = new Listener(shape)
            listener.hoverCursor()
            listener.addListener(listener.event('click'), () => {
                this.clickLine(key)
            })
        })
        return shapes
    }

    drawBalons(images, trackSet) {
        let initialBalon = images.find(balon => balon.id === 35)
        let balons = images.filter(image => {
            return trackSet.fruits.some(fruit => {
                return `balon-${fruit}` === image.name
            })
        })
        for (let i = balons.length; i < 5; i++) {
            balons.push(initialBalon)
        }
        balons = this.render.renderBalons(balons)
        balons.forEach((balon, key) => {
            let listener = new Listener(balon)
            listener.addListener(listener.event('mouseover'), () => {
                Anim.setCursor('pointer')
                balon.tweenRotate.play()
            })
            listener.addListener(listener.event('mouseout'), () => {
                Anim.setCursor('default')
                balon.tweenRotate.reverse()
            })
            listener.addListener(listener.event('click'), () => {
                this.clickLine(key)
            })
        })
        return balons
    }

    /**
     * Отрисовка падающих фруктов
     * @param images
     * @param gameSettings
     * @returns {Array} KonvaObj
     */
    drawGameItems(images, gameSettings, trackSet) {
        // let fruit = images.find(image => Number(image.id) === 3)
        // let gameObjects = images.filter(image => {
        //     let id = Number(image.id)
        //     return id === 3 || id === 4 || id === 5 || id === 6 || id === 7
        // })
        let initialObject = images.find(image => image.id === 4)

        let gameObjects = images.filter(image => {
            return trackSet.fruits.some(fruit => {
                return fruit === image.name
            })
        })
        for (let i = gameObjects.length; i < 5; i++) {
            gameObjects.push(initialObject)
        }
        return this.render.renderGameItems(gameObjects, gameSettings)
    }

    drawSpray(images) {
        let spray = images.find(image => Number(image.id) === 9)
        return this.render.renderSpray(spray)
    }

    drawFruitSpray(images) {
        let spray = images.find(image => Number(image.id) === 10)
        return this.render.renderFruitSpray(spray)
    }

    clickLine(idLine) {
        this.mehanic.lineClick(idLine)
    }
}
