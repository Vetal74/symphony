export default class TimeShuffle {
    constructor(settings) {
        this.musicTimeDiff = settings.musicTimeDiff

    }

    /**
     * Время начала игры
     */
    markStartMusicTime() {
        this.startMusicTime = performance.now()
    }
}
