import TimeShuffle from './time-shuffle'
import Animations from '../canvas/animations'
export default class Mehanic {
    constructor(params) {
        this.textMotivation = params.textMotivation
        this.audio = params.audio
        this.gameItems = params.gameItems
        this.trackMap = params.trackMap
        this.gameSettings = params.gameSettings
        this.nextPointTrack = 0
        this.throwedItem = 0
        this.score = 0
        this.shapes = params.shapes
        this.scoreText = params.textRender({
            text: 'Очки\n 0',
            x: 898,
            y: 10,
            fontSize: 20,
            align: 'center',
            fontFamily: 'Marta-Italic',
            fill: 'white'
        })
        this.audioPshik = params.audioPshik
        this.balons = params.balons
        this.sprays = params.sprays
        this.fruitSprays = params.fruitSprays
        this.progressLine = params.progressLine

        const maxValuePoint = 109
        const maxValueHeight = 400
        const countPointItems = this.trackMap.length
        const maxValueScore = maxValuePoint * countPointItems
        this.maxValueScore = maxValueScore
        this.valueHeight = maxValueScore / maxValueHeight

        this.isDesktop = screen.width > 1000

        this.audio.on('end', () => {
            this.audio.unload()
        })

        // Fruit animation end
        const cloneendEvent = (e) => {
            this.fruitEnd(e.detail.clone)
        }
        document.removeEventListener('cloneEnd', cloneendEvent, true)
        document.addEventListener('cloneEnd', cloneendEvent)

        // Fruit animation end
        const fruitClickEvent = (e) => {
            this.lineClick(e.detail.key)
        }
        document.removeEventListener('fruitClick', fruitClickEvent, true)
        document.addEventListener('fruitClick', fruitClickEvent)

        let pauseButton = document.querySelector('#symphony-app .pause-button')
        const pauseEvent = e => {
            let layer = document.querySelector('#symphony-app .symphony-layer')
            let pause = document.querySelector('#symphony-app .symphony-pause')
            pauseButton.querySelector('.pause-but').classList.remove('active')
            pauseButton.querySelector('.play-but').classList.add('active')
            pause.classList.add('active')
            layer.classList.add('active')

            if (this.audio) {
                this.audio.pause()
            }
            this.destroyTicker()
            this.stopAllItems()
            this.stopGame = true
        }
        pauseButton.removeEventListener('click', pauseEvent, true)
        pauseButton.addEventListener('click', pauseEvent)
    }

    getPixelsAsScore(scoreValue) {
        return scoreValue / this.valueHeight
    }

    destroy() {
        document.removeEventListener('cloneEnd', (e) => {
            this.fruitEnd(e.detail.clone)
        })
        this.textMotivation = 0
        if (this.audio) {
            this.audio.unload()
        }
        this.audio = 0
        this.gameItems = 0
        this.trackMap = 0
        this.gameSettings = 0
        this.nextPointTrack = 0
        this.throwedItem = 0
        this.score = 0
        this.shapes = 0
        this.scoreText = 0
        if (this.audioPshik) {
            this.audioPshik.unload()
        }
        this.audioPshik = 0
        this.balons = 0
        this.sprays = 0
        this.fruitSprays = 0
        this.progressLine = 0
    }

    /**
     * Запускаем шарманку
     */
    start() {
        let pauseButton = document.querySelector('#symphony-app .pause-button')
        let layer = document.querySelector('#symphony-app .symphony-layer')
        let pause = document.querySelector('#symphony-app .symphony-pause')
        let logout = document.querySelector('#symphony-app .symphony-pause p')
        pause.classList.add('active')
        layer.classList.add('active')
        let self = this
        function logoutEvent(e) {
            pauseButton.classList.add('active')
            pauseButton.querySelector('.pause-but').classList.add('active')
            pauseButton.querySelector('.play-but').classList.remove('active')
            pause.classList.remove('active')
            layer.classList.remove('active')
            self.leaveGame()
        }
        logout.onclick = logoutEvent
        // logout.addEventListener('click', logoutEvent)
        const pauseEvent = e => {
            pauseButton.classList.add('active')

            pauseButton.querySelector('.pause-but').classList.add('active')
            pauseButton.querySelector('.play-but').classList.remove('active')
            pause.classList.remove('active')
            layer.classList.remove('active')
            if (!this.startTime) {
                this.startTime = performance.now()
                setTimeout(() => {
                    this.audio.play()
                }, (this.trackMap[0].time) * 1000)
                this.pause = false
                this.computeThrowItems()
            } else {
                this.audio.play()
                this.refresher()
                this.playAllItems()
            }
            this.stopGame = false
        }
        pause.querySelector('img').removeEventListener('click', pauseEvent, true)
        pause.querySelector('img').onclick = pauseEvent
    }

    refresher() {
        this.pause = false
        this.timeDiffPostPause = performance.now()
        this.iterator()
    }

    fruitEnd(clone) {
        this.cloneStopped = clone
        if (this.audio) {
            this.audio.pause()
        }
        this.destroyTicker()
        this.stopAllItems()
    }

    /**
     * Тап линии
     * @param idLine
     */
    lineClick(idLine) {
        if (this.stopGame) {
            return
        }
        this.audioPshik.play()
        Animations.balonRotate(this.balons[idLine])
        if (this.isDesktop) {
            setTimeout(() => {
                Animations.pushSpray(this.sprays[idLine])
            }, 70)
        }

        const tappedLine = idLine + 1

        let findTrack = this.trackMap[this.nextPointTrack]

        if (!this.trackMap[this.nextPointTrack + 1] || !this.trackMap[this.nextPointTrack]) {
            this.endGame()
            return false
        }

        let successFruit = () => {
            let result = false
            this.gameItems[idLine].forEach(clone => {
                let frameTime = clone.tweenTranslate.anim.frame.time
                if (frameTime > 1600 &&
                    frameTime < 2800 && !result) {
                    if (frameTime >= 1650 && frameTime < 1750) { // Хорошо
                        result = 40 * (frameTime / 1000)
                        this.textMotivation[3].tweenVisible.play()
                    }
                    if (frameTime < 1650) { // Рано
                        result = 20 * (frameTime / 1000)
                        this.textMotivation[1].tweenVisible.play()
                    }
                    if (frameTime >= 1750 && frameTime < 1800) { // Идеально1
                        result = 60 * (frameTime / 1000)
                        this.textMotivation[4].tweenVisible.play()
                    }
                    if (frameTime >= 1800 && frameTime < 1850) { // Идеально2
                        result = 60 * (1820 / 1000)
                        this.textMotivation[4].tweenVisible.play()
                    }
                    if (frameTime >= 1850 && frameTime <= 1900) { // Идеально3
                        result = 60 * (1750 / 1000)
                        this.textMotivation[4].tweenVisible.play()
                    }
                    if (frameTime > 1900) { // Поздно
                        result = 30 * (frameTime / 1000)
                        this.textMotivation[5].tweenVisible.play()
                    }
                    clone.tweenTranslate.anim.frame.time = 0
                    clone.tweenTranslate.reset()
                    return false
                }
            })
            return result
        }

        // console.log('tapped:', tappedLine, 'need:', findTrack.line)
        if (!this.pause) { // Паузы игры не было и клик удачный
            let result = successFruit()
            if (!result) {
                this.score -= 75
                if (this.score < 0) {
                    this.score = 0
                }
                this.scoreText.setText(`Очки\n ${this.score}`)
                if (this.isDesktop) {
                    Animations.shapeFail(this.shapes[idLine])
                    setTimeout(() => {
                        Animations.refreshProgress(this.progressLine, this.getPixelsAsScore(this.score))
                    }, 70)
                    this.textMotivation[0].tweenVisible.play()
                }
                return false
            } else {
                this.nextPointTrack++
                this.score += Math.floor(result)
                this.scoreText.setText(`Очки\n ${this.score}`)
                if (this.isDesktop) {
                    setTimeout(() => {
                        Animations.pushFruitSpray(this.fruitSprays[idLine])
                        Animations.refreshProgress(this.progressLine, this.getPixelsAsScore(this.score))
                    }, 70)
                }
                return true
            }
        }
        if (this.pause) { // Была пауза и тап удачный
            this.nextPointTrack++
            this.audio.play()
            this.cloneStopped.tweenTranslate.anim.frame.time = 0
            this.cloneStopped.tweenTranslate.reset()
            this.refresher()
            this.playAllItems()
            if (this.isDesktop) {
                this.textMotivation[5].tweenVisible.play()
                setTimeout(() => {
                    // Animations.shapeSuccess(this.shapes[idLine])
                    Animations.pushFruitSpray(this.fruitSprays[idLine])
                    Animations.refreshProgress(this.progressLine, this.getPixelsAsScore(this.score))
                }, 70)
            }
            return true
        }
        if (this.isDesktop) {
            Animations.shapeFail(this.shapes[idLine])
        }
    }

    destroyTicker() {
        this.pause = true
        this.timePostPause = (performance.now() - this.timePendingTimeout) / 1000
        clearTimeout(this.throwTimer)
    }

    getNowTime() {
        return (performance.now() - this.startTime) / 1000
    }

    computeThrowItems() {
        setTimeout(() => {
            this.thrower(this.trackMap[this.throwedItem])
            this.iterator()
        }, 100)
    }

    iterator() {
        this.timeCalc().then(() => this.iterator())
    }

    thrower(item) {
        let selected = false
        this.gameItems[item.line - 1].forEach(cloneItem => {
            if (cloneItem.tweenTranslate.tween._time === 0 && !selected) {
                cloneItem.tweenTranslate.play()
                selected = true // Запутанная история, но так надо
                this.throwedItem++
                return false
            }
        })
    }

    timeCalc() {
        return new Promise(resolve => {
            if (this.throwedItem >= this.trackMap.length) {
                return false
            }
            let timeDiff = this.trackMap[this.throwedItem].time - this.trackMap[this.throwedItem - 1].time
            if (this.timePostPause) {
                timeDiff -= this.timePostPause
            }
            this.timePendingTimeout = performance.now()
            if (this.pause) {
                return false
            }
            this.throwTimer = setTimeout(() => {
                this.trackMap[this.throwedItem].status = 'pending'
                if (!this.trackMap[this.throwedItem + 1]) {
                    setTimeout(() => {
                        this.endGame()
                        return false
                    }, 2200)
                }
                this.thrower(this.trackMap[this.throwedItem])
                this.timePostPause = 0
                resolve()
            }, timeDiff * 1000)
        })
    }

    endGame() {
        if (this.pause) {
            return
        }
        let event = new CustomEvent('gameEnd', {
            detail: {
                score: this.score,
                maxScore: this.maxValueScore
            }
        })
        document.dispatchEvent(event)
        if (this.audio) {
            this.audio.pause()
        }
        this.destroyTicker()
        this.stopAllItems()
    }

    leaveGame() {
        let event = new CustomEvent('leaveGame')
        document.dispatchEvent(event)
        if (this.audio) {
            this.audio.pause()
        }
        this.destroyTicker()
        this.stopAllItems()
    }

    /**
     * Остановить все элементы
     */
    stopAllItems() {
        this.gameItems.forEach(item => {
            item.forEach(clone => clone.tweenTranslate.pause())
        })
    }

    /**
     * Запустить все элементы
     */
    playAllItems() {
        this.gameItems.forEach(item => {
            item.forEach(clone => {
                if (clone.tweenTranslate.tween._time > 0) {
                    clone.tweenTranslate.play()
                }
            })
        })
    }
}
