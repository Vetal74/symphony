import SymphonyApi from './index'

export default class Loader {
    constructor(configName) {
        this.configName = configName
        this.config = SymphonyApi.getSettings(configName)
        this.category = ''
    }

    /**
     * Получение всех полей
     * @returns {Promise}
     */
    getAttrs() {
        return this.config
    }

    /**
     * Получение объекта конфига по его id
     * @param id
     * @returns {Promise}
     */
    getAttrById(id) {
        return this.config.then(result => {
            if (id === undefined) {
                throw new Error('Не передан id')
            }
            let response = result.find(obj => obj.id === id)
            if (response) {
                return response
            } else {
                throw new Error('Не найдены свойства по id')
            }
        }).catch(err => console.error(err))
    }

    /**
     * Получение атрибута по его имени
     * @param params
     * @param.id - id объекта
     * @param.configObject - объект
     * @param.name - имя атрибута
     * @returns {*}
     */
    getAttrByName(params = {}) {
        let configObject = {}
        if (params.id !== undefined) {
            configObject = this.getAttrById(params.id)
        } else if (params.configObject) {
            configObject = params.configObject
        } else {
            throw new Error('Не найдены передаваемые свойства')
        }
        if (configObject instanceof Promise) {
            return configObject.then(result => result[params.name])
        } else if (configObject[params.name] === undefined) {
            throw new Error(`Объект ${params.name} отсутсвует в ${this.configName}`)
        }
        return new Promise(resolve => resolve(configObject[params.name]))
    }

    /**
     * Получение объектов конфига по категории
     * @param category - имя категории
     * @returns {Promise}
     */
    getObjectsByCategory(category) {
        this.category = category
        return this.config.then(config => {
            let response = config.filter(object => object.category === category)
            if (response) {
                return response
            } else {
                throw new Error(`Ничего не найдено в ${this.configName} по категории ${category}`)
            }
        }).catch(err => console.error(err))
    }

    /**
     * Загружаем изображения клиенту
     * @param images
     * @returns {Promise.<*>}
     */
    loadImages(images) {
        try {
            const load = image => {
                let img = new Image()
                img.src = image.src
                image.imageObj = img
                return new Promise(resolve => {
                    img.onload = () => resolve(image)
                })
            }
            if (typeof (images) === Object) {
                return load(images.src)
            } else {
                let promises = images.map(image => load(image))
                return Promise.all(promises)
            }
        } catch (err) {
            console.log(err)
        }
    }
}
