import qs from 'query-string'
export default class SymphonyApi {
    constructor() {
    }

    /**
     * Запрос к API
     * @param method
     * @param href
     * @param params
     */
    static requestFromApi(method = 'GET', href = '', params = {}) {
        let get = method === 'GET'
        let promise = {}
        if (get) {
            promise = fetch(href, Object.assign(params, {}))
        } else {
            promise = fetch(href, {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                body: qs.stringify(params)
            })
        }
        return promise.then(response => response.json())
    }

    static getFromApi(href = '', params = {}) {
        return this.requestFromApi('GET', href, params)
    }

    static setFromApi(href = '', params = {}) {
        return this.requestFromApi('POST', href, params)
    }

    /**
     * Получение настроек
     * @param name
     * @returns {*}
     */
    static getSettings(name = 'settings') {
        return import(`./../../../src/settings/${name}.json`)
    }
}
