import {Howl, Howler} from 'howler'
export default class AudioShaker {
    constructor(audioPath, settings, volume = 1) {
        this.audio = new Howl({
            src: audioPath,
            loop: false,
            volume
        })
        return new Promise(resolve => {
            this.audio.on('load', () => {
                resolve(this.audio)
            })
        })
    }

    volume(value) {
        this.audio.volume = value
    }

    play() {
        this.audio.play()
    }

    pause() {
        this.audio.pause()
    }
}
