export default {
    state: {
        events: [
            {
                name: 'startGame',
                event: {}
            }, {
                name: 'pauseGame',
                event: {}
            }, {
                name: 'fruitEnd',
                event: {}
            }
        ]
    },
    getters: {
        events() {
            console.log(this)
        }
    }
}
