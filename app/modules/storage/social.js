export default class Social {
    /**
     *
     * @param type - className social icon
     * @param e event
     */
    static shareSocial(type, e = 0, href) {
        if (e !== 0) {
            e.preventDefault()
        }
        const frameShare = this.getTemplateSocial(type, href)
        this.openFrame(frameShare)
    }

    static getTemplateSocial(type, href) {
        let url = ''
        let title = ''
        switch (type) {
            case 'social--vk':
                title = 'Поделиться страницой в VK'
                url = this.shareVk(href)
                break
            case 'social--facebook':
                title = 'Поделиться страницой в Facebook'
                url = this.shareFacebook(href)
                break
            case 'social--ok':
                title = 'Поделиться страницой в Однокласниках'
                url = this.shareOk(href)
                break
            case 'social--twitter':
                title = 'Поделиться страницой в Twitter'
                url = this.shareTwitter(href)
                break
            default:
                throw new Error('Не передали тип социальной кнопки')
        }
        return {
            template: this.templateShare(url),
            url: url,
            title: title
        }
    }

    static openFrame(frame) {
        const windowOpen = window.open(frame.url, frame.title, 'width=300,height=400,left=200,top=200')
        windowOpen.onload = () => {
            windowOpen.document.write(frame.template)
        }
    }

    static templateShare(url) {
        return `<iframe src="${url}" width="137" height="20" style="border:none;overflow:hidden" 
scrolling="no" frameborder="0" allowTransparency="true"></iframe>`
    }

    static shareVk(href = window.location.href) {
        return `https://vk.com/share.php?url=${href}`
    }

    static shareFacebook(href = window.location.href) {
        return `https://www.facebook.com/sharer/sharer.php?u=${href}`
    }

    static shareOk(href = window.location.href) {
        return `http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=${href}`
    }

    static shareTwitter(href = window.location.href) {
        return `http://twitter.com/share?url=${href}`
    }
}
