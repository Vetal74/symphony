export default {
    BackEaseIn(t, b, c, d) {
        let s = 1.70158
        return c * (t /= d) * t * ((s + 1) * t - s) + b
    }
}