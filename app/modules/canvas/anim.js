import Konva from 'konva'
export default class Anim {
    constructor(object, layer, params) {
        this.object = object
        this.layer = layer
        this.params = params
    }

    static setCursor(type) {
        document.body.style.cursor = type
    }

    scale() {
        let animation = new Konva.Animation(frame => {
            let scale = Math.sin(frame.time * 2 * Math.PI / this.params.duration) + 0.001
            this.object.scale({x: scale, y: scale})
        }, this.layer)
        return animation
    }

    tween(easing = 'Linear') {
        let tweenParams = {
            node: this.object
        }
        let params = Object.assign(tweenParams, this.params, {easing: Konva.Easings[easing]})
        return new Konva.Tween(params)
    }
}