import Anim from './anim'
import Listener from './../listener/index'
import Store from './../storage/index'
export default class Animations {
    constructor() {
    }

    /**
     * Анимирование элеметнов на главном экране
     * @param images
     * @param canvas {Canvas}
     */
    setAnimationMainScreen(images, canvas) {
        let needImage = images.filter(image => image.props.podcat === 'game-preview')
        let layer = canvas.getLayer(needImage[0].props.zIndex)
        needImage.forEach(nImage => {
            canvas.setCenter(nImage, {x: 'center', y: 'center'})
            nImage.tween = new Anim(nImage, layer, {
                scaleX: 1.1,
                scaleY: 1.1,
                duration: 0.2
            }).tween()
            const listener = new Listener(nImage)
            listener.addListener(listener.event('mouseover'), () => {
                Anim.setCursor('pointer')
                nImage.tween.play()
            })
            listener.addListener(listener.event('mouseout'), () => {
                Anim.setCursor('default')
                nImage.tween.reverse()
            })
            listener.addListener(listener.event('click'), () => {
                let event = new CustomEvent(Store.state.events[0].name, {
                    detail: {
                        nImage
                    }
                })
                document.dispatchEvent(event)
            })
        })
    }

    static balonRotate(balon) {
        balon.tweenRotate.play()
        setTimeout(() => {
            balon.tweenRotate.reverse()
        }, 250)
    }

    static pushSpray(spray) {
        spray.tweenPop.play()
    }

    static pushFruitSpray(fruitSpray) {
        fruitSpray.tweenPop.play()
    }

    static refreshProgress(line, val) {
        line.height(val)
        line.fillLinearGradientEndPointY(val)
        line.offsetY(val)
        line.draw()
    }

    static shapeSuccess(shape) {
        shape.tweenSuccess.play()
    }

    static shapeFail(shape) {
        shape.tweenFail.play()
    }
}
