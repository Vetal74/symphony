import Konva from 'konva'

export default class Canvas {
    constructor(width = 980, height = 600, container = 'symphony-game') {
        this.stage = new Konva.Stage({width, height, container})
        this.container = container
        this.stageWidth = 980
        this.stageHeight = 600
        this.initLayers()
        if (screen.width < 800) {
            this.responsive()
        }
        window.addEventListener('resize', () => {
            this.responsive()
        })
    }

    responsive() {
        const isHorizontal = () => (screen.width > screen.height)
        if (!isHorizontal()) {
            document.querySelector('.low-screen').classList.add('active')
        } else {
            document.querySelector('.low-screen').classList.remove('active')
        }
        let container = document.getElementById(this.container)
        // now we need to fit stage into parent
        let containerWidth = container.offsetWidth
        // to do this we need to scale the stage
        let scale = containerWidth / this.stageWidth
        this.stage.width(this.stageWidth * scale)
        this.stage.height(this.stageHeight * scale)
        this.stage.scale({x: scale, y: scale})
        this.stage.draw()
    }

    initLayers() {
        this.backLayer = new Konva.Layer()
        this.menuLayer = new Konva.Layer()
        this.gameLayer = new Konva.Layer()
        this.overGameLayer = new Konva.Layer()
        this.sprayFruitsLayer = new Konva.Layer()
        this.resultGameLayer = new Konva.Layer()
    }

    /**
     * Отрисовка изображения
     * @param {Image} img
     * @param {Object} coords
     * @param {Object} props
     */
    drawImageToCanvas(img, coords = {}, props = {}, zIndex = 1, id = 0, rotation = 0) {
        try {
            if (!(img instanceof Image)) {
                throw new Error('Не указана ссылка на image')
            }
            if (coords.x === undefined || coords.y === undefined) {
                throw new Error('Не указаны координаты изображения')
            }
            const checkCoord = (d, key) => {
                if (typeof d !== 'number') {
                    coords[key] = this.coordConverter(d, key, props)
                }
            }
            const checkSize = size => {
                if (size.scale === '1') {
                    return true
                }
                props = this.sizeConverter(props)
            }
            checkSize(props)
            checkCoord(coords.x, 'x')
            checkCoord(coords.y, 'y')

            const needLayer = this.getLayer(zIndex)

            let konvaImage = new Konva.Image({
                x: coords.x,
                y: coords.y,
                image: img,
                width: props.width,
                height: props.height,
                offset: {
                    x: 0,
                    y: 0
                },
                preventDefault: false,
                rotation
            })
            konvaImage.setAttr('id', id)
            needLayer.add(konvaImage)
            this.stage.add(needLayer)
            return konvaImage
        } catch (err) {
            console.error(err)
        }
    }

    drawLine(params = {}, zIndex) {
        let line = new Konva.Line(params)
        let layer = this.getLayer(zIndex)
        layer.add(line)
        this.stage.add(layer)
    }

    drawCircle(params = {}, zIndex) {
        let circle = new Konva.Circle(params)
        let layer = this.getLayer(zIndex)
        layer.add(circle)
        this.stage.add(layer)
        return circle
    }

    drawRectangle(params = {}) {
        let rect = new Konva.Rect({
            x: params.position.x,
            y: params.position.y,
            height: params.size.height,
            width: params.size.width,
            fill: params.styles.fill
        })
        let layer = this.getLayer(params.zIndex)
        layer.add(rect)
        this.stage.add(layer)
        return rect
    }

    getLayer(zIndex) {
        switch (zIndex) {
            case 0:
                return this.backLayer
            case 1:
                return this.menuLayer
            case 2:
                return this.gameLayer
            case 3:
                return this.overGameLayer
            case 4:
                return this.sprayFruitsLayer
            case 5:
                return this.resultGameLayer
            default:
                return this.backLayer
        }
    }

    getStage() {
        return this.stage
    }

    removeLayer(zIndex) {
        let layer = this.getLayer(zIndex)
        layer.destroy()
    }

    /**
     * Центр трансформации объекта
     * @param object
     * @param offset
     */
    setCenter(object, offset = {}) {
        let position = object.position()
        if (offset.x === 'center') {
            const set = object.width() / 2
            object.offsetX(set)
            position.x += set
        } else {
            object.offsetX(offset.x)
            position.x += offset.x
        }
        if (offset.y === 'center') {
            const set = object.height() / 2
            object.offsetY(set)
            position.y += set
        } else {
            object.offsetY(offset.y)
            position.y += offset.y
        }

        object.position(position)

        object.parent.draw()
    }

    /**
     * вывод текста на холст
     * @param {object} text
     */
    drawText(params, zIndex) {
        let konvaText = new Konva.Text(params)
        let layer = this.getLayer(zIndex)
        layer.add(konvaText)
        this.stage.add(layer)
        return konvaText
    }

    /**
     * Преобразователь координат
     * @param d - величина оси
     * @param nameD - имя оси x или y
     * @param propSize - размеры
     * @returns {number}
     */
    coordConverter(d, nameD, propSize) {
        const keyAxis = nameD === 'x' ? 'width' : 'height'
        let axis = this.stage[keyAxis]()
        switch (d) {
            case 'center':
                return axis / 2 - propSize[keyAxis] / 2
        }
    }

    sizeConverter(size) {
        switch (size.scale) {
            case 'full':
                return {
                    scale: size.scale,
                    width: this.stage.width(),
                    height: this.stage.height()
                }
            default:
                const scale = Number(size.scale)
                return {
                    scale,
                    width: size.width * scale,
                    height: size.height * scale
                }
        }
    }

    /**
     * Очищаем канвас
     */
    clearCanvas() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    }
}