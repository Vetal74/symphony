const path = require('path');
// TODO: dopilit config :)
module.exports = {
    entry: './app/script.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist/'),
        publicPath: '/symphony/dist/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015'], plugins: ['syntax-dynamic-import']
                    }
                }
            }
        ]
    },
    watch: true,
    devtool: 'source-map'
};